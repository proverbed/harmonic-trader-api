'use strict';

// This file is used to export the model Schemas
const Bar = require('./Bar');
const Setup = require('./Setup');

// Export all the schema for ease of access.
module.exports = {
  Bar: Bar,
  Setup: Setup
};