'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BarSchema = new Schema(
  {
    'open': {
      type: Number,
      required: true
    },
    'close': {
      type: Number,
      required: true
    },
    'high': {
      type: Number,
      required: true
    },
    'low': {
      type: Number,
      required: true
    },
    'datetime': {
      type: Date,
      required: true
    },
    'instrument': {
      type: String,
      required: true
    },
    'timeframe': {
      type: String,
      required: true
    }
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
    collection: 'Bar'
  }
);

/**
 * Defines the schema for a Bar
 *
 * @author Dmitri De Klerk <dmitriwarren@gmail.com>
 * @since  22 June 2017
 *
 * @module Bar
 */
module.exports = mongoose.model('Bar', BarSchema);