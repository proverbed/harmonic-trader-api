'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PointSchema = new Schema(
  {
    price: {
      type: Number,
      required: true
    },
    datetime: {
      type: Date,
      required: true
    }
  }
);

let SetupSchema = new Schema(
  {
    'name': {
      type: String,
      required: true
    },
    'x': PointSchema,
    'a': PointSchema,
    'b': PointSchema,
    'c': PointSchema,
    'd': PointSchema,
    'instrument': {
      type: String,
      required: true
    },
    'timeframe': {
      type: String,
      required: true
    }
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
    collection: 'Setup'
  }
);

/**
 * Defines the schema for a Setup
 *
 * @author Dmitri De Klerk <dmitriwarren@gmail.com>
 * @since  09 July 2017
 *
 * @module Setup
 */
module.exports = mongoose.model('Setup', SetupSchema);