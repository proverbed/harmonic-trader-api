'use strict';

const {Bar} = require('../../api/models');
const moment = require('moment');
const RuntimeError = require('../../api/lib/error/RuntimeError');

class BarService {

  /**
   * Constructor used to set the logger instance
   *
   * @param {object} logger - A logger instance
   */
  constructor(logger) {
    this.logger = logger;
  }

  /**
   * Create a new bar
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  createBar(swaggerParams, res, next) {
    let instrument = swaggerParams.instrument.value;
    let timeframe = swaggerParams.timeframe.value;
    let payload = swaggerParams.createBar.value;
    payload.instrument = instrument;
    payload.timeframe = timeframe;
    let datetime = payload.datetime;
    let bar = new Bar(payload);

    let query = {
      instrument: instrument,
      timeframe: timeframe,
      datetime: datetime
    };

    Bar.find(query).exec(
      (error, response) => {
        if (error) {
          let runTimeError = new RuntimeError(
            'Error trying to check if the the Bar already exist',
            error
          );
          this.logger.error('Runtime Error while trying to check if the the Bar already exist');
          return next(runTimeError);
        }

        if (response.length == 0) {
          bar.save((error, response) => {
            if (error) {
              let runTimeError = new RuntimeError(
                'Error trying to save the new Bar',
                error
              );
              this.logger.error('Runtime Error while trying to save the new Bar');
              return next(runTimeError);
            }

            this.logger.info('New bar created');
            res.statusCode = 201;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(response));
          });
        } else {
          res.statusCode = 409;
          res.setHeader('Content-Type', 'application/json');
          res.setHeader('X-Result-Count', response.length);
          res.end(JSON.stringify(payload));
        }
      });
  }

  /**
   * Retrieve a listing of bars for the given instrument and timeframe
   *
   * This route supports query params
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveBar(swaggerParams, res, next) {
    let instrument = swaggerParams.instrument.value;
    let timeframe = swaggerParams.timeframe.value;
    let page = swaggerParams.page.value;
    let perPage = swaggerParams.per_page.value;
    let skip = ((page - 1) * perPage);
    let fromDate = swaggerParams.from_date.value;
    let toDate = swaggerParams.to_date.value;
    let latestOnly = swaggerParams.latest_only.value;
    let sortDatetime = 1;

    // Only return the latest bar
    if (latestOnly) {
      perPage = 1; // limit to one record
      skip = 0;
      sortDatetime = -1;
    }

    let options = {
      skip: skip, // Starting Row
      limit: perPage, // Ending Row, 1 shoud do
      sort: {
        datetime: sortDatetime // sort by datetime ASC
      }
    };
    let query = {
      instrument: instrument,
      timeframe: timeframe
    };
    let datetimeQuery = {};
    if (fromDate) {
      datetimeQuery.$gte = moment(fromDate).toDate();
    }
    if (toDate) {
      datetimeQuery.$lte = moment(toDate).toDate();
    }
    // if either from or to date have been set, set this as a query condition
    if (datetimeQuery.$gte || datetimeQuery.$lte) {
      query.datetime = datetimeQuery;
    }
    Bar.find(
      query, // Search Filter
      null, //  Return all Columns
      options,
      (error, response) => {
        if (error) {
          let runTimeError = new RuntimeError(
            'Error trying to retrieve the Bar details',
            error
          );
          this.logger.error('Runtime Error while trying to retrieve the Bar details');
          return next(runTimeError);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.setHeader('X-Result-Count', response.length);
        res.end(JSON.stringify(response));
      });
  }
}

module.exports = BarService;