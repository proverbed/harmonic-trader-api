'use strict';

const {Bar, Setup} = require('../../api/models');
const moment = require('moment');
const RuntimeError = require('../../api/lib/error/RuntimeError');
const _ = require('lodash');

class SetupService {

  /**
   * Constructor used to set the logger instance
   *
   * @param {object} logger - A logger instance
   */
  constructor(logger) {
    this.logger = logger;
  }

  /**
   * Create a new setup
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  createSetup(swaggerParams, res, next) {
    let instrument = swaggerParams.instrument.value;
    let timeframe = swaggerParams.timeframe.value;
    let payload = swaggerParams.createSetup.value;
    payload.instrument = instrument;
    payload.timeframe = timeframe;
    let name = payload.name;
    let x = payload.x;
    let a = payload.a;
    let b = payload.b;
    let c = payload.c;
    let d = payload.d;
    let setup = new Setup(payload);

    let query = {
      instrument: instrument,
      timeframe: timeframe,
      name: name,
      'x.datetime': x.datetime,
      'a.datetime': a.datetime,
      'b.datetime': b.datetime,
      'c.datetime': c.datetime,
      'd.datetime': d.datetime
    };

    Setup.find(query).exec(
      (error, response) => {
        if (error) {
          let runTimeError = new RuntimeError(
            'Error trying to check if the the Setup already exist',
            error
          );
          this.logger.error('Runtime Error while trying to check if the the Setup already exist');
          return next(runTimeError);
        }

        if (response.length == 0) {
          setup.save((error, response) => {
            if (error) {
              let runTimeError = new RuntimeError(
                'Error trying to save the new Setup',
                error
              );
              this.logger.error('Runtime Error while trying to save the new Setup');
              return next(runTimeError);
            }

            this.logger.info('New setup created');
            res.statusCode = 201;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(response));
          });
        } else {
          res.statusCode = 409;
          res.setHeader('Content-Type', 'application/json');
          res.setHeader('X-Result-Count', response.length);
          res.end(JSON.stringify(payload));
        }
      });
  }

  /**
   * Retrieve a listing of setups for the given instrument and timeframe
   *
   * This route supports query params
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveSetupListing(swaggerParams, res, next) {
    let instrument = swaggerParams.instrument.value;
    let timeframe = swaggerParams.timeframe.value;
    let page = swaggerParams.page.value;
    let perPage = swaggerParams.per_page.value;
    let skip = ((page - 1) * perPage);

    let options = {
      skip: skip, // Starting Row
      limit: perPage // Ending Row, 1 shoud do
    };
    let query = {
      instrument: instrument,
      timeframe: timeframe
    };

    let fields = {
      updated_at: 1,
      created_at: 1,
      name: 1
    };

    Setup.find(
      query, // Search Filter
      fields,
      options,
      (error, response) => {
        if (error) {
          let runTimeError = new RuntimeError(
            'Error trying to retrieve the Setup details',
            error
          );
          this.logger.error('Runtime Error while trying to retrieve the Setup details');
          return next(runTimeError);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.setHeader('X-Result-Count', response.length);
        res.end(JSON.stringify(response));
      });
  }

  /**
   * Retrieve a setups for the given instrument and timeframe
   *
   * This route supports query params
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveSetup(swaggerParams, res, next) {
    let instrument = swaggerParams.instrument.value;
    let timeframe = swaggerParams.timeframe.value;
    let id = swaggerParams.id.value;

    let options = {
    };
    let query = {
      _id: id,
      instrument: instrument,
      timeframe: timeframe
    };

    Setup.findOne(
      query, // Search Filter
      null,
      options,
      (error, response) => {
        if (error) {
          let runTimeError = new RuntimeError(
            'Error trying to retrieve the Setup details',
            error
          );
          this.logger.error('Runtime Error while trying to retrieve the Setup details');
          return next(runTimeError);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        let queryBar = {
          instrument: instrument,
          timeframe: timeframe,
          datetime: {
            $gte: moment(response.x.datetime).toDate(),
            $lte: moment(response.d.datetime).toDate()
          }
        };

        Bar.find(
          queryBar, // Search Filter
          {
            updated_at: 0,
            created_at: 0,
            __v: 0,
            instrument: 0,
            timeframe: 0
          }, //  Return all Columns
          {
            sort: {
              datetime: 1
            }
          },
          (error, barResponse) => {
            if (error) {
              let runTimeError = new RuntimeError(
                'Error trying to retrieve the Bar details for a Setup',
                error
              );
              this.logger.error('Runtime Error while trying to retrieve the Bar details for a Setup');
              return next(runTimeError);
            }

            let result = {
              Setup: response,
              Bars: barResponse
            };

            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(result));
          });
      });
  }
}

module.exports = SetupService;