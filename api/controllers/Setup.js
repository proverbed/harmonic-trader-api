'use strict';

const SetupService = require('../../api/services/SetupService');

/**
 * Create a new setup
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.createSetup = function createSetup(req, res, next) {
  let setupService = new SetupService(req.Logger);
  setupService.createSetup(req.swagger.params, res, next);
};

/**
 * Retrieve the latest setup listing
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveSetupListing = function retrieveSetupListing(req, res, next) {
  let setupService = new SetupService(req.Logger);
  setupService.retrieveSetupListing(req.swagger.params, res, next);
};

/**
 * Retrieve the setup details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveSetup = function retrieveSetup(req, res, next) {
  let setupService = new SetupService(req.Logger);
  setupService.retrieveSetup(req.swagger.params, res, next);
};