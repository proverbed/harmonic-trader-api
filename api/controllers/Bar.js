'use strict';

const BarService = require('../../api/services/BarService');

/**
 * Create a new bar
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.createBar = function createBar(req, res, next) {
  let barService = new BarService(req.Logger);
  barService.createBar(req.swagger.params, res, next);
};

/**
 * Retrieve the latest bar
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveBar = function retrieveBar(req, res, next) {
  let barService = new BarService(req.Logger);
  barService.retrieveBar(req.swagger.params, res, next);
};
