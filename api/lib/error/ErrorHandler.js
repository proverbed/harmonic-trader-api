'use strict';

const ServerError = require('./ServerError');
const RuntimeError = require('./RuntimeError');
const _ = require('lodash');

/**
 * The module used for building and formatting the applications http error responses
 *
 * @module ErrorHandler
 */
module.exports = {

  /**
   * This builds up a new error using the passed in validation error object.
   *
   * This creates a uniform structure for all our validation error responses
   *
   * @param {Error} error - The error object that will be prepared for display
   * @param {ClientRequest} request - The http request object
   * @param {IncomingMessage} response - The http response object
   * @param {function} next The callback used to pass control to the next action/middleware
   * @param {Object} config - The config
   */
  onError: function onError(error, request, response, next, config) {
    if (!request.Logger) {
      throw new RuntimeError('The Logger is not set on the request');
    }
    if (error.status) {
      config = _getConfig(config, error.status);
      error = _prepareError(error, response, error.status, config);

    } else if (response.statusCode) {
      config = _getConfig(config, response.statusCode);
      error = _prepareError(error, response, response.statusCode, config);
    } else {
      // this means that it is not a validation or customs error so it has to be an internal server error
      error = _prepareServerErrorForDisplay(error, response);
    }

    var logging = request.Logger;
    if (error.originalError) {
      var logError = {};
      logError['original'] = error.originalError;
      delete error.originalError;
      logError['error'] = error;

      logging.logAction(logging.logLevels.ERROR, 'An error occurred', logError);
    } else {
      logging.logAction(logging.logLevels.ERROR, 'An error occurred', error);
    }

    var displayError = _prepareDisplayErrorPerEnvironment(error, config);

    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(displayError));
  }
};

/**
 * This builds up a new error using the passed in error object.
 *
 * Here we can override the code for different error types
 *
 * @private
 *
 * @param {Error} error - The error object that will be prepared for display
 * @param {IncomingMessage} response - The http response object
 * @param {Number} statusCode - The status code which will be used to determine the type of the error
 *
 * @returns {Error} The error that has been prepared for display
 */
function _prepareError(error, response, statusCode, config) {
  // This is to change the status code to 406 since it has been incorrectly set to 400 by the lib we are using
  if (!_.isUndefined(error.message) && error.message.indexOf('Invalid content type') >= 0) {
    statusCode = 406;
  }
  if (config['client_errors'] && config['client_errors'].hasOwnProperty(statusCode)) {
    if (!_.isUndefined(config['client_errors'][statusCode])) {
      error.code = config['client_errors'][statusCode];
      error = _prepareClientErrorForDisplay(error);
      response.statusCode = statusCode;
    } else {
      error = _prepareClientErrorForDisplay(error);
      response.statusCode = statusCode;
    }
    return error;
  }

  if (config['server_errors'] && Array.isArray(config['server_errors'])) {
    if (config['server_errors'].indexOf(statusCode) !== -1) {
      error = _prepareServerErrorForDisplay(error, response, statusCode);
    } else {
      error = _prepareServerErrorForDisplay(error, response);
    }
  }
  return error;
}

/**
 * This builds up a new Client side error using the passed in error object.
 *
 * @private
 *
 * @param {Error} error - The error object that will be prepared for display
 *
 * @returns {Object} The error that has been prepared for display
 */
function _prepareClientErrorForDisplay(error) {
  var errForDisplay = {};
  if (error.code) {
    errForDisplay.code = error.code;
  } else {
    errForDisplay.code = 'VALIDATION_ERROR';
  }

  if (error.message) {
    errForDisplay.message = error.message;
  }
  if (error.results) {
    errForDisplay.errors = error.results.errors;
  }
  if (error.exceptionCode) {
    errForDisplay.exceptionCode = error.exceptionCode;
  }
  return errForDisplay;
}

/**
 * This builds up a new Server side error using the passed in error object.
 *
 * @private
 *
 * @param {Error} error - The error object that will be prepared for display
 * @param {IncomingMessage} response - The http response object
 *
 * @returns {Object} The error that has been prepared for display
 */
function _prepareServerErrorForDisplay(error, response, statusCode) {
  if (statusCode) {
    response.statusCode = statusCode;
  } else {
    response.statusCode = 500;
  }
  return new ServerError(error, error.originalError);
}

/**
 * This will determine which config to use when building up this error.
 *
 * @private
 *
 * @param {Object} config - The config
 * @param {Number} statusCode - The status code
 *
 * @returns {Object} The config
 */
function _getConfig(config) {
  var clientErrors = {
    '404': 'RESOURCE_NOT_FOUND',
    '405': 'METHOD_NOT_SUPPORTED',
    '406': 'CONTENT_TYPE_NOT_SUPPORTED',
    '400': null,
    '409': null,
    '413': null
  };
  var serverErrors = [504, 502, 500];

  if (!config || (config && Object.keys(config).length === 0)) {
    config.client_errors = clientErrors;
    config.server_errors = serverErrors;
    return config;
  }

  if (!config['client_errors']) {
    config.client_errors = clientErrors;
  }

  if (!config['server_errors']) {
    config.server_errors = serverErrors;
  }

  return config;
}

/**
 * This will remove properties from the error objects according to whether we allow debug properties or not based
 * on the environmental configuration.
 *
 * @param {object} error - The error that needs to be checked.
 *
 * @private
 *
 * @returns {object} The checked error.
 */
function _prepareDisplayErrorPerEnvironment(error, config) {
  // Remove the stack if it is there and debug is false.
  if (!config.bDebug && error.stack) {
    delete error.stack;
  }
  if (!config.bDebug && !_.isUndefined(error.stackTrace)) {
    delete error.stackTrace;
  }

  return error;
}