'use strict';

var chai = require('chai');
var assert = chai.assert;
var _ = require('lodash');

module.exports = {

  /**
   * Assert that the runtime error is as expected.
   * The stackTrace and stack properties will be removed from the objects before using `assert.deepEqual`.
   *
   * @param {Object} actual - The actual data to compare
   * @param {Object} expected - The expected data to compare
   * @param {string} message - The message to show when the assert fails
   */
  deepEqual: function equal(actual, expected, message) {
    if (!_.isNil(actual.stackTrace)) {
      delete actual.stackTrace;
    }
    if (!_.isNil(expected.stackTrace)) {
      delete expected.stackTrace;
    }
    if (!_.isNil(actual.originalError) && !_.isNil(actual.originalError.stackTrace)) {
      delete actual.originalError.stackTrace;
    }
    if (!_.isNil(expected.originalError) && !_.isNil(expected.originalError.stackTrace)) {
      delete expected.originalError.stackTrace;
    }

    assert.deepEqual(
      actual,
      expected,
      message
    );
  }
};
