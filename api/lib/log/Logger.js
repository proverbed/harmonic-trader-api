'use strict';

var Winston = require('winston');
var uuid = require('uuid');
var _ = require('lodash');

var logger = null;
var stack = null;

/**
 * Logger class that encapsulates a winston logger and allows the creation of a logging context.
 */
class Logger {
  /**
   * Does the setup for the logger.
   *
   * @param {*} options - Options object for the logger that is created
   *
   * @return void
   */
  static setup(options) {

    // Set options to an empty object if it is not defined.
    if (!options) {
      options = {};
    }
    var transports = [
      new Winston.transports.Console({
        json: true,
        colorize: false,
        timestamp: true,
        stringify: options.pretty ? null : JSON.stringify
      })
    ];

    if (options.transports) {
      transports = options.transports;
    }

    if (options.stack) {
      stack = options.stack;
    }

    logger = new Winston.Logger({
      level: options.level || 'info',
      levels: Winston.config.syslog.levels,
      transports: transports
    });

    _.forEach(logger.levels, function createContextShorthandFunctions(level, name) {
      LoggerContext.prototype[name] = function log(message, object, callback) {
        this.log(name, message, object, callback);
      };
    });
  }

  /**
   * This will flush all the logger transport to ensure all logs are persisted.
   * This function should be called be the application exists.
   *
   * @param Function callback - The function that should be called once all log message are persisted
   *
   * @return void
   */
  static flushLogger(callback) {
    logger.log('info', 'flush logger', stack, callback);
  }

  /**
   * Creates a logger context with the given context id
   *
   * @param string requestId - The context id
   *
   * @return LoggerContext
   */
  static getContext(requestId) {
    return new LoggerContext(requestId);
  }
}

/**
 * Logger context class ensures all messages logged from a specific logger context
 * has the same context id so messages from
 * the same process can found in the logging system.
 */
class LoggerContext {

  /**
   * Constructs an instance of the LoggerContext class.
   *
   * @param string requestId - (optional) If the context id is not passed in an context id will be generated.
   *
   * @return LoggerContext - Instance of LoggerContext
   */
  constructor(requestId) {
    if (!requestId) {
      this.requestId = uuid.v4();
    } else {
      this.requestId = requestId;
    }
  }

  /**
   * Property that exposes log level names as constants
   */
  get logLevels() {
    return {
      INFO: 'info',
      CRITICAL: 'crit',
      ERROR: 'error',
      WARN: 'warn',
      NOTICE: 'notice',
      DEBUG: 'debug',
      EMERGENCY: 'emerg',
      ALERT: 'alert'
    };
  }

  /**
   * Adds a log message with a given log level.
   *
   * @param string level - The log level of the message
   * @param string message - The message that should be logged
   * @param {*} objectOrCallback - Meta object or callback
   * @param Function callback - optional that will be called if the message has been persisted by all logger Transports
   *
   * @return void
   */
  logAction(level, message, objectOrCallback, callback) {
    this.log(level, message, objectOrCallback, callback);
  }

  /**
   * Adds a log message with a given log level.
   *
   * @param string level - The log level of the message
   * @param string message - The message that should be logged
   * @param {*} objectOrCallback - Meta object or callback
   * @param Function callback - optional that will be called if the message has been persisted by all logger Transports
   *
   * @return void
   */
  log(level, message, objectOrCallback, callback) {
    var object = objectOrCallback;
    if (_.isFunction(objectOrCallback)) {
      callback = objectOrCallback;
      object = {};
    }

    if (object instanceof Error) {
      object = parseError(object);
    } else if (object && object.error instanceof Error) {
      object = _.extend({}, object, parseError(object.error));
      delete object.error;
    }

    var messageObject = _.extend({}, stack || {}, object);
    messageObject.requestId = this.requestId;

    if (callback) {
      logger.log(level, message, messageObject, callback);
    } else {
      logger.log(level, message, messageObject);
    }
  }
}

/**
 * Parses the error object into an object that can be used by our logging systems.
 * This will check to see it the error has an original error connected to it.
 *
 * @param {Error} error - The error that should be parsed
 *
 * @return {object} - The object that will used by the logger
 */
function parseError(error) {
  let errorObject;

  if (error.original) {
    errorObject = parseErrorParts(error.original);
  }

  errorObject = parseErrorParts(error, errorObject);

  return errorObject;
}

/**
 * Parses the error object into an object that can be used by our logging systems.
 *
 * @param {Error} error - The error that should be parsed
 *
 * @return {object} - The object that will used by the logger
 */
function parseErrorParts(error, errorObject) {
  if (!errorObject) {
    errorObject = {};
  }

  if (!errorObject.code && error.code) {
    errorObject.errorCode = error.code;
  }
  // Attempt to retrieve the error message
  if (!errorObject.errorMessage && error.message) {
    errorObject.errorMessage = error.message;
  }
  // Attempt to retrieve the stack trace from the stack property
  if (!errorObject.errorStack && error.stack) {
    errorObject.errorStack = error.stack;
  }
  // If we could not retrieve the stack trace off of the stack property we try the stackTrace property
  if (!errorObject.errorStack && error.stackTrace) {
    errorObject.errorStack = error.stackTrace;
  }

  return errorObject;
}

module.exports = Logger;