Place configuration files in this directory.


## DB HOST

```yml
"database_host": "mongodb://mongodb:27017/harmonicTrader",
```

The reference: mongodb, instead of the IP, refers to the name of the container within the `docker-compose.yml`