'use strict';

const Logger = require('./api/lib/log/Logger');
const config = require('config');
const mongoose = require('mongoose');
const app = require('express')();
const http = require('http');
const swaggerTools = require('swagger-tools');
const jsyaml = require('js-yaml');
const fs = require('fs');
const cors = require('cors');
const ErrorHandler = require('./api/lib/error/ErrorHandler');
const serverPort = (config.has('server.port')) ? config.get('server.port') : 3000;
mongoose.Promise = global.Promise;

Logger.setup(config.logger);

// swaggerRouter configuration
const options = {
  swaggerUi: '/swagger.json',
  controllers: './api/controllers',
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

const errorHandlerConfig = {
  client_errors: config.get('client_errors')
};

mongoose.connect(config.mongo.database_host, config.mongo.options);
mongoose.set('debug', config.mongo.debug);
mongoose.connection.on(
  'error',
  function mongooseConnection(error) {
    let loggerContext = Logger.getContext('startup');
    loggerContext.info('MongoDB connection error', error);
    process.exit(1);
  }
);

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
// eslint-disable-next-line no-sync
const spec = fs.readFileSync('./api/swagger/swagger.yaml', 'utf8');
const swaggerDoc = jsyaml.safeLoad(spec);

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function middleWareFunc(middleware) {

  app.use(function initUse(req, res, next) {
    let loggerContext = Logger.getContext();

    loggerContext.info('Request started', {
      method: req.method,
      url: req.url
    });
    req.Logger = loggerContext;
    next();
  });

  app.use(cors(config.get('cors')));

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  app.use(function errorHandler(err, req, res, next) {
    ErrorHandler.onError(err, req, res, next, errorHandlerConfig);
  });

  // Start the server
  http.createServer(app).listen(serverPort, function createFunc() {
    // eslint-disable-next-line no-console
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
    // eslint-disable-next-line no-console
    console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
  });
});

module.exports = app;// for testing purposes