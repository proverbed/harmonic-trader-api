FROM node:boron-alpine

RUN echo "hello from node borron"

# Install app dependencies
COPY package.json ./
# Keep this one command, to not expose the secret.
RUN  npm install

# Add the application source code
COPY . ./

EXPOSE 3010

CMD ["node", "app.js"]

# Build docker images in local repository
# docker build  . -t harmonic-trader-api