'use strict';

// The file where all index for Bar
// eslint-disable-next-line no-undef
db.Setup.ensureIndex(
  {
    'x.datetime': 1,
    'a.datetime': 1,
    'b.datetime': 1,
    'c.datetime': 1,
    'd.datetime': 1,
    'name': 1,
    'instrument': 1,
    'timeframe': 1
  },
  {
    unique: true
  }
);