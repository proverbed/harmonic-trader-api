'use strict';

// The file where all index for Bar
// eslint-disable-next-line no-undef
db.Bar.ensureIndex(
  {
    'datetime': 1,
    'instrument': 1,
    'timeframe': 1
  },
  {
    unique: true
  }
);