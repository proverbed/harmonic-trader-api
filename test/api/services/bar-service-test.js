'use strict';

const {expect, assert} = require('chai');
const sinon = require('sinon');
const {Bar} = require('../../../api/models');
const BarService = require('../../../api/services/BarService');
const RuntimeErrorAssert = require('../../../api/lib/error/RuntimeErrorAssert');
const RuntimeError = require('../../../api/lib/error/RuntimeError');

describe('BarService test scenarios', () => {
  let logger = {
    getLogger: function logger(logSpy, requestId) {
      return {
        'debug': logSpy,
        'info': logSpy,
        'error': logSpy,
        'logAction': logSpy,
        'requestId': requestId
      };
    }
  };
  describe('createBar(): Create a bar', () => {

    /**
     * Test the successful creation of a new Bar
     *
     * @covers services/BarService.createBar
     */
    it('201 response test', (done) => {
      let statusCode = 201;
      let expectedHeaders = [
        {'Content-Type': 'application/json'}
      ];
      let saveResponse = new Bar({
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-06-23T12:05:00.000Z',
        'instrument': 'USD/JPY',
        'timeframe': 'M5'
      });
      let payload = {
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-06-23T14:55:17.250Z'
      };

      let args = {
        createBar: {
          value: payload
        },
        instrument: 'USD/JPY',
        timeframe: 'M5'
      };

      let actualHeaders = [];
      let nextSpy = sinon.spy();
      let logSpy = sinon.spy();
      let resSetHeaderSpy = sinon.spy(
        (header, value) => {
          let obj = {};
          obj[header] = value;
          actualHeaders.push(obj);
        }
      );

      let resEndSpy = sinon.spy((response) => {
        assert.deepEqual(
          response,
          JSON.stringify(saveResponse)
        );
        assert.deepEqual(
          actualHeaders,
          expectedHeaders,
          'The incorrect headers where set for the response'
        );
        assert.equal(
          res.statusCode,
          statusCode,
          'The incorrect status code is set when there is no content'
        );
        expect(logSpy.callCount).to.equal(1);
        expect(nextSpy.callCount).to.equal(0);
        expect(resEndSpy.callCount).to.equal(1);
        expect(barSave.callCount).to.equal(1);
        done();
        barSave.restore();
      });

      let res = {statusCode: '', setHeader: resSetHeaderSpy, end: resEndSpy};
      let barSave = sinon.stub(Bar.prototype, 'save', (callback) => {
        callback(null, saveResponse);
      });

      let barService = new BarService(logger.getLogger(logSpy));
      barService.createBar(args, res, nextSpy);
    });

    /**
     * Test the run time error when trying to create a Bar
     *
     * @covers services/BarService.createBar
     */
    it('Run time error test', (done) => {
      let payload = {
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-06-23T14:55:17.250Z'
      };

      let args = {
        createBar: {
          value: payload
        },
        instrument: 'USD/JPY',
        timeframe: 'M5'
      };

      let errObject = {some: 'error'};
      let runtimeError = new RuntimeError(
        'Error trying to save the new Bar',
        errObject
      );
      let nextSpy = sinon.spy((error) => {
        RuntimeErrorAssert.deepEqual(
          error,
          runtimeError,
          'The expected runtime error was not returned'
        );
        expect(logSpy.callCount).to.equal(1);
        expect(nextSpy.callCount).to.equal(1);
        expect(resEndSpy.callCount).to.equal(0);
        done();
        barSave.restore();
      });
      let logSpy = sinon.spy();
      let resSetHeaderSpy = sinon.spy();
      let resEndSpy = sinon.spy();

      let res = {statusCode: '', setHeader: resSetHeaderSpy, end: resEndSpy};
      let barSave = sinon.stub(Bar.prototype, 'save', (callback) => {
        callback(errObject, null);
      });

      let barService = new BarService(logger.getLogger(logSpy));
      barService.createBar(args, res, nextSpy);
    });
  });

});