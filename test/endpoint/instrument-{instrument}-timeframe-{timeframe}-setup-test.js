'use strict';
var chai = require('chai');
var ZSchema = require('z-schema');
var validator = new ZSchema({});
var request = require('supertest');
var server = require('../../app');
const _ = require('lodash');

chai.should();

describe('/instrument/{instrument}/timeframe/{timeframe}/setup', () => {
  var instrument = 'instrument_' + _.random(1.0, 500.0);
  var timeframe = 'timeframe_' + _.random(1.0, 500.0);

  describe('post', () => {
    it('should respond with 201 Return the setup object...', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "x",
          "a",
          "b",
          "c",
          "d",
          "name"
        ],
        "properties": {
          "x": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "a": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "b": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "c": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "d": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "name": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'x': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'a': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'b': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'c': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'd': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'name': 'Gartley'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/setup')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(201)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

    it('should respond with 400 Validation Error. Usually...', function (done) {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "errors": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [
                "code",
                "message",
                "path"
              ],
              "properties": {
                "code": {
                  "type": "string"
                },
                "message": {
                  "type": "string"
                },
                "path": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "description": {
                  "type": "string"
                }
              }
            }
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'x': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'a': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'b': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'c': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'd': {
          'price': 0,
          'datetime': 'invalid'
        },
        'name': 'Gartley'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/setup')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(400)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

    it('should respond with 409 Conflict error, when the...', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "x",
          "a",
          "b",
          "c",
          "d",
          "name"
        ],
        "properties": {
          "x": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "a": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "b": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "c": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "d": {
            "type": "object",
            "properties": {
              "price": {
                "type": "number",
                "format": "double"
              },
              "datetime": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "name": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'x': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'a': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'b': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'c': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'd': {
          'price': 0,
          'datetime': '2017-07-10T16:40:59.000Z'
        },
        'name': 'Gartley'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/setup')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(409)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

  });

});
