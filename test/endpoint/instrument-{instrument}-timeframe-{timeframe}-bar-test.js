'use strict';
var chai = require('chai');
var ZSchema = require('z-schema');
var validator = new ZSchema({});
var request = require('supertest');
var server = require('../../app');
const _ = require('lodash');

chai.should();

describe('/instrument/{instrument}/timeframe/{timeframe}/bar', () => {
  var instrument = 'instrument_' + _.random(1.0, 500.0);
  var timeframe = 'timeframe_' + _.random(1.0, 500.0);

  describe('post', () => {
    it('should respond with 201 Return the bar object...', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "open",
          "close",
          "high",
          "low",
          "datetime"
        ],
        "properties": {
          "open": {
            "type": "number",
            "format": "double"
          },
          "close": {
            "type": "number",
            "format": "double"
          },
          "high": {
            "type": "number",
            "format": "double"
          },
          "low": {
            "type": "number",
            "format": "double"
          },
          "datetime": {
            "type": "string",
            "format": "date-time"
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-07-23T14:55:10.000Z'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(201)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

    it('should respond with 400 Validation Error. Usually...', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "errors": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [
                "code",
                "message",
                "path"
              ],
              "properties": {
                "code": {
                  "type": "string"
                },
                "message": {
                  "type": "string"
                },
                "path": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "description": {
                  "type": "string"
                }
              }
            }
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'open': 0,
        'close': 0,
        'high': 'invalid',
        'low': 0,
        'datetime': '2017-07-23T14:55:10.000Z'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(400)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

    it('should respond with 409 Conflict error, when the...', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "open",
          "close",
          "high",
          "low",
          "datetime"
        ],
        "properties": {
          "open": {
            "type": "number",
            "format": "double"
          },
          "close": {
            "type": "number",
            "format": "double"
          },
          "high": {
            "type": "number",
            "format": "double"
          },
          "low": {
            "type": "number",
            "format": "double"
          },
          "datetime": {
            "type": "string",
            "format": "date-time"
          }
        }
      };

      /*eslint-enable*/
      let payload = {
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-07-23T14:55:10.000Z'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(409)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

  });

  describe('get', () => {
    it('should respond with 200 Return the bar details', (done) => {
      /*eslint-disable*/
      var schema = {
        "type": "array",
        "items": {
          "allOf": [
            {
              "type": "object",
              "required": [
                "open",
                "close",
                "high",
                "low",
                "datetime"
              ],
              "properties": {
                "open": {
                  "type": "number",
                  "format": "double"
                },
                "close": {
                  "type": "number",
                  "format": "double"
                },
                "high": {
                  "type": "number",
                  "format": "double"
                },
                "low": {
                  "type": "number",
                  "format": "double"
                },
                "datetime": {
                  "type": "string",
                  "format": "date-time"
                }
              }
            }
          ],
          "properties": {
            "_id": {
              "type": "string"
            }
          }
        }
      };

      /*eslint-enable*/
      let instrument = 'instrument_' + _.random(1.0, 500.0);
      let timeframe = 'timeframe_' + _.random(1.0, 500.0);

      /*eslint-enable*/
      let payload = {
        'open': 0,
        'close': 0,
        'high': 0,
        'low': 0,
        'datetime': '2017-07-23T14:55:10.000Z'
      };
      request(server)
        .post('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .set('Accept', 'application/json')
        .send(payload)
        .expect(201)
        .end((err, res) => {
          if (err) {return done(err);}

          request(server)
            .get('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
            .set('Accept', 'application/json')
            .expect(200)
            .end((err, res) => {
              if (err) {return done(err);}

              validator.validate(res.body, schema).should.be.true;
              done();
            });
        });
    });

    it('should respond with 204 No Content. There were no...', (done) => {
      let instrument = 'instrument_' + _.random(1.0, 500.0);
      let timeframe = 'timeframe_' + _.random(1.0, 500.0);

      request(server)
        .get('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .set('Accept', 'application/json')
        .expect(204)
        .end((err, res) => {
          if (err) {return done(err);}

          res.body.should.equal(''); // non-json response or no schema
          done();
        });
    });

    it('should respond with 400 Validation Error. Usually...', (done) =>{
      /*eslint-disable*/
      var schema = {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "string"
          },
          "message": {
            "type": "string"
          },
          "errors": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [
                "code",
                "message",
                "path"
              ],
              "properties": {
                "code": {
                  "type": "string"
                },
                "message": {
                  "type": "string"
                },
                "path": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "description": {
                  "type": "string"
                }
              }
            }
          }
        }
      };

      /*eslint-enable*/
      request(server)
        .get('/instrument/'+ instrument +'/timeframe/' + timeframe + '/bar')
        .query({
          from_date: 'DATA GOES HERE',
          to_date: 'DATA GOES HERE',
          latest_only: 'DATA GOES HERE',
          page: 'DATA GOES HERE',
          per_page: 'DATA GOES HERE'
        })
        .set('Accept', 'application/json')
        .expect(400)
        .end((err, res) => {
          if (err) {return done(err);}

          validator.validate(res.body, schema).should.be.true;
          done();
        });
    });

  });

});
