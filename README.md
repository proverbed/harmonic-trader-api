## Harmonic Trader API

This will be the api for the harmonic trader

Running the application 

## Start the server

```
cd "C:\Users\Dmitri\Documents\Bitbucket\harmonic-trader-api" && 
forever start 
 -l "C:\Users\Dmitri\Documents\Bitbucket\harmonic-trader-api\app-forever.log" -a 
 -o "C:\Users\Dmitri\Documents\Bitbucket\harmonic-trader-api\app-output.log"
 -e forever-error.log
 -c node app.js
```

## stop all forever processes 

```
forever stopall
```